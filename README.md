# ESP32 Camera Door Bell


Roadmap:
* Click on the button to send an alert to openHab
* A video stream that show the camera image
* Messages on the screen showing the current state of the application
* Detection sensor that automatically trigger the alert
* Automatic door open with a button in openHab

## Flows
###Configuration flow:

```mermaid
graph TD
    wifi[Configuring Wifi]
    mqtt[Configuring MQTT]
    waitButton[Waiting for ring]
    InitalState --> wifi
    wifi --> mqtt
    mqtt --> waitButton
 ```
 
 Ring flow:

```mermaid
graph TD
    waitButton[Waiting for ring]
    push[Push MQTT Ring Message]
    wait[Wait MQTT Answer Message]
    answered[Answered]
    noanswer[No answser received for 10 seconds]
    waitButton --> push
    push --> wait
    wait --> answered
    wait --> noanswer
    noanswer --> waitButton
```

 Camera flow:

```mermaid
graph TD
    stream[Stream video]
    wait[Wait MQTT Camera Message]
    wait --> stream
```

 Open Door flow:

```mermaid
graph TD
    open[Open Door]
    wait[Wait MQTT Open Message]
    wait --> open
```

Credits:
* openHab: https://www.openhab.org/
* https://github.com/lewisxhe/esp32-camera-series